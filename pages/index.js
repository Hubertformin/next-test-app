import Head from 'next/head'
import React, {useState, useEffect} from 'react'
import Post from '../components/post'
import styles from '../styles/Home.module.css'

export default function Home() {
  const [posts, setPosts] = useState([
    {username: 'Hubert Formin', caption: 'Yoo bro!!'},
      {username: 'Marcus Formin', caption: 'Yes bro!!'}
  ]);
  // useEffect -> runs a piece of code based on a specific condition
  // example usage: runs a piece of code, when page refreshes or when comment is added to site
  // if no condition is specified, it's only going to run once
  // if the input
  useEffect(() => {
    // db.collection('posts').onSnapshot(snapshot => {
    //   setPosts(snapshot.docs.map(doc => doc.data()));
    // })
  }, []); // this not only going to run once, but it will run each time the variable post changes, if post is specified in array

  return (
    <div className={styles.container}>
      <Head>
        <title>Hubert Formins App</title>
        <meta name="title" content="Hubert Formins App" />
        <meta name="description" content="With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://metatags.io/" />
        <meta property="og:title" content="Hubert Formins App" />
        <meta property="og:description" content="With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!" />
        <meta property="og:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png" />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://metatags.io/" />
        <meta property="twitter:title" content="Hubert Formins App" />
        <meta property="twitter:description" content="With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!" />
        <meta property="twitter:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.header}>
          <img src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png" />
        </div>
        
        {
          posts.map(post => {
            <Post key={post.id} username={post.username} caption={post.caption} />
            /** key on react components, help tell react not to re-render element if posts array update:
             * This helps with performance
             */
          })
        }

        <a href="/about">About</a>
      </main>
    </div>
  )
}
