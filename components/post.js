import React from 'react'

function Post({username, caption}) {
    return (
        <div>
            <h3>{username}</h3>
    <p>{caption}</p>
        </div>
    );
}

export default Post
